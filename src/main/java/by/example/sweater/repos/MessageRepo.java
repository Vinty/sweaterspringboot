package by.example.sweater.repos;

import by.example.sweater.domain.Message;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepo extends CrudRepository <Message, Long> {

}
