package by.example.sweater;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {
    @GetMapping("/greeting")
    public String greeting(
            @RequestParam(name = "myName", required = false, defaultValue = "World") String name1,
            Model model) {
        model.addAttribute("name", name1);
        return "greeting";
    }

    @GetMapping
    public String main(Model model) {
        model.addAttribute("some", "hello!");
        return "main";
    }
}
